# llm-sandboxes


## Large Language Model Sandboxes

This is a suite of JupyterLab/Voila notebooks for testing and demonstrating some LLM model capabilities.

You can either run the Jupyter notebooks from a JupyterLab instance, or use the excellent [Voila](https://voila.readthedocs.io/) package to render the notebooks as standalone applications.

To make the infrastructure needed to run these notebooks portable, this project also contains the source to build a Docker container capable of running JupyterLab/Voila, and instructions on how to use docker-compose to orchestrate the Jupyter container with an nginx container to provide a secure connection to the environment

Note that you will need your own Azure OpenAI key to run the sandboxes since they use Azure LLM services.

## Description
Sandboxes:
- *00_Completions_sandbox.ipynb* 
-- enter some text and a query, get an LLM response. Uswe a slider to control the temperature of the LLM's completion 
 
- *01_PDF_query_sandbox.ipynb* 
-- upload a PDF file, the file is split into pages, and you can query the LLM about the first 4 pages

- *02_PDF_Embedding_analysis_sandbox.ipynb* 
-- upload a PDF file, the file is split with a Sentence Transformer, embeddings are calculated on the splits and stopred on a Chroma vector database. When you make a query, an embedding is calculated on your query, and the 4 best matches in the vector database are used in the prompt to the LLM. This illustrates a common RAG (Retrieval Augmented Generation) pattern allowing you to query your document
 
- *03_PDF_FAIIS_analysis_sandbox.ipynb* 
-- upload a PDF file, the file is split and embeddings are calculated on the splits using [FAISS](https://github.com/facebookresearch/faiss). When you make a query, an embedding is calculated on your query, and the 4 best matches are used in the prompt to the LLM. This illustrates doing RAG using Facebook AI Similarity Search against your document
 
- *04_GLiNER_text_sandbox.ipynb* 
-- An example of a Name Entity Retrieval (NER) engine. In this example we use [GLiNER](https://github.com/urchade/GLiNER) a [Generalist and Lightweight Model for Named Entity Recognition](https://arxiv.org/abs/2311.08526) because it outperforms both ChatGPT and fine-tuned LLMs in zero-shot evaluations.

- *05_Knowledge_graph_sandbox.ipynb* 
-- An example of Knowledge Graph RAG. The knowledge graph was built by extracting entity-relationship->entity triples from a 422 page introduction to statistics textbook [OpenIntro Statistics](https://www.openintro.org/book/os/). The triples are stored in a Nebula Graph database and this knowledge graph is then used in hybrid search mode (searching both by keyword match in the knowledge graph and by similarity) to identify documents chunks to be used to augment ChatGPT. The initial one-time build of the knowledge graph takes ~20 minutes. To see how the Knowledge graph was built see the notebook *setup-open_statistics-nebulagraph.ipynb*. 

- *06_OpenIntro_Statistics_sandbox.ipynb* 
-- An example of calculating embeddings once and saving them so that loading the app is fast. The document here is a 422 page introduction to statistics textbook [OpenIntro Statistics](https://www.openintro.org/book/os/) available under a Creative Commons license. A possible next step here would be to display the portions of the text that are identified as semantically close as PDF rather then plain text -- but that will have to wait. 


## Examples

### Sentence transformer RAG with Chroma vector database

[<img src="images/Sentence_Transformer_example.jpg" width=200 height=200>](images/Sentence_Transformer_example.jpg)
 
### GLiNER named entity retrival

[<img src="images/GLiNER_example.jpg" width=200 height=200>](images/GLiNER_example.jpg)

## Installation

To maintain compatibility with the coursework containers run at cmgr.oit.duke.edu, some of the paths here may seem a bit idiocyncratic. Chalk it up to backward compatibility with a convention established about 8 years ago... and the possibility that some version of this container could become a course container at some point.


Docker-compose.yml makes assumptions to be aware of:


The persistent home directory for the container user is assumed to be located here:
````
     /srv/persistent-data/homedirs/user001
````
The home directory should be owned by UID 1000 and GID 1000, so you'll want to set that up before docker-compose runs to avoid having a read-only home directory for the user in the container. After you set it up, copy the notebooks into the home directory.


An environment file with key configuration settings for the Jupyter containers is assumed to be located here:
````
/srv/persistent-data/homedirs/configs/faiss_config/config
````
This is where your OpenAPI key is specified along with the models and base URL. An example of this file is found in the config subdirectory of this project.


A password for the Jupyter instance is set in the docker-compose.yml file on this line:
````
      - PASSWORD=xxx_YOUR_SECRET_PASSWORD_GOES_HERE_xxx
````
This is the password your are prompted to enter when you first visit this container's web page.


The docker-compose.yml file assumes that the config files for the nginx container live on this path:
````
     /srv/persistent-data/docker-scripts/nginx
````
A template for those configs is found in the nginx directory of the repository, along with a README with a bit more detail


The knowledge graph example notebook assumes you have a local Nebula Graph database running. The easy way to do this is to go to the nebula subdirectory of this project and start nebula like this:
````
     sudo docker-compose -f docker-compose-studio.yml up
````
This will launch the database and a web GUI console at port 7001 which you can log into as 'root' using any password - you almost certainly do not want to expose this to the internet at large ;-). You also probably don't want the other nebula services exposed to the internet, so a host-based firewall would be a good idea here - just make sure that the host where the jupyterlab notebooks is run has access to the database.

## Example Configuration

````
    NOTEBOOK_ARGS="--VoilaConfiguration.show_tracebacks=true"
    OPENAPI_API_TYPE="azure"
    OPENAPI_API_BASE="https://jupyter-openai-canada-east.openai.azure.com/"
    OPENAPI_API_VERSION="2024-02-15-preview"
    OPENAPI_API_KEY="xxx_your-personal-openai-key-goes-here_xxx"
````

## Usage

````
sudo docker-compose up
````

Then go to https://YOUR_HOSTNAME_GOES_HERE/ and enter the password for Jupyter from docker-compose.yml

To see the Voila version of the notebooks visit 
````
    https://YOUR_HOSTNAME_GOES_HERE/voila
````
or click on the Voila icon while running a notebook in JupyterLab mode like this:
 
 ![click here to render in Voila](images/click-here.jpg)